package epam.com;
import java.util.List;
/**
 * Products from plumbing category.
 * Class extends Hypermarket.
 */
public class Plumbing extends Hypermarket {


    Plumbing(final List<String> nameOfProducts,
        final int[] priceOfProducts, final int[] countOfProducts) {
        super(nameOfProducts, priceOfProducts, countOfProducts);
    }
}
