package epam.com;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that has main function to test
 * all classes.
 */
public class Programm {

    public static void main(String[] args) {
        List<String>  plumbingProducts = new ArrayList<String>();
        plumbingProducts.add("Washing machine");
        plumbingProducts.add("Toilet");
        plumbingProducts.add("Washing machine");
        int[] priceOfPlumbingProducts = new int[plumbingProducts.size()];
        priceOfPlumbingProducts[0] = 150;
        priceOfPlumbingProducts[1] = 25;
        priceOfPlumbingProducts[2] = 250;
        int[] countOfPlumbingProducts = new int[plumbingProducts.size()];
        countOfPlumbingProducts[0] = 2;
        countOfPlumbingProducts[1] = 3;
        countOfPlumbingProducts[2] = 4;

        Plumbing test =
            new Plumbing(plumbingProducts, priceOfPlumbingProducts,
                countOfPlumbingProducts);
        test.products();
        test.findProductByPrice(160);
        test.findByName("Stiralka");

        List<String> kitchenProducts = new ArrayList<String>();
        kitchenProducts.add("Rag");
        kitchenProducts.add("Table");
        kitchenProducts.add("Desk");
        int[] priceOfProductsInKitchen = new int[kitchenProducts.size()];
        priceOfProductsInKitchen[0] = 120;
        priceOfProductsInKitchen[1] = 250;
        priceOfProductsInKitchen[2] = 190;
        int[] countOfProductsInKitchen = new int[kitchenProducts.size()];
        countOfProductsInKitchen[0] = 5;
        countOfProductsInKitchen[1] = 4;
        countOfProductsInKitchen[2] = 3;

        Hypermarket test1 =
            new ProductsForKitchen(kitchenProducts, priceOfProductsInKitchen,
                countOfProductsInKitchen);
        test1.products();
        test1.findProductByPrice(190);
        test1.findByName("Table");

        List<String> woodProducts = new ArrayList<String>();
        woodProducts.add("Cupboard");
        woodProducts.add("Laminate");
        int[] priceOfProductsFromWood = new int[woodProducts.size()];
        priceOfProductsFromWood[0] = 500;
        priceOfProductsFromWood[1] = 300;
        int[] countOfProductsFromWood = new int[woodProducts.size()];
        countOfProductsFromWood[0] = 2;
        countOfProductsFromWood[1] = 100;
        Hypermarket productsFromWood =
            new ProductFromWood(woodProducts, priceOfProductsFromWood,
                countOfProductsFromWood);
        productsFromWood.findByName("Cupboard");
        productsFromWood.findProductByPrice(700);

        List<String> decorativeProducts = new ArrayList<String>();
        decorativeProducts.add("Painting");
        decorativeProducts.add("Vase");
        int priceDecorativeProducts[] = new int[decorativeProducts.size()];
        priceDecorativeProducts[0] = 1000;
        priceDecorativeProducts[1] = 250;
        int countOfDecorativeProducts[] = new int[decorativeProducts.size()];
        countOfDecorativeProducts[0] = 25;
        countOfDecorativeProducts[1] = 50;

        Hypermarket decorativeProductsObj =
            new DecorativeProducts(decorativeProducts, priceDecorativeProducts,
                countOfDecorativeProducts);
        decorativeProductsObj.findProductByPrice(40);
        decorativeProductsObj.findByName("Statuette");
    }
}
