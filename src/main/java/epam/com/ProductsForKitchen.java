package epam.com;
import java.util.List;

/**
 * Products from kitchen category.
 * Class extends Hypermarket.
 */
public class ProductsForKitchen extends Hypermarket {

  ProductsForKitchen(final List<  String> nameOfProducts,
      final int[] priceOfProducts, int[] countOfProducts) {
    super(nameOfProducts, priceOfProducts, countOfProducts);
  }

}