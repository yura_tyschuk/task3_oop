package epam.com;
import java.util.List;

/**
 * Products from wood category.
 * Class extends Hypermarket.
 */
public class ProductFromWood extends Hypermarket {

    ProductFromWood(final List<String> nameOfProducts,
        final int[] priceOfProducts, final int[] countOfProducts) {
        super(nameOfProducts, priceOfProducts, countOfProducts);
    }

}
