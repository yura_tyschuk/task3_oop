package epam.com;
import java.util.List;

/**
 * Products from decorative category.
 * Class extends Hypermarket.
 */
public class DecorativeProducts extends Hypermarket{
    DecorativeProducts(final List<String> nameOfProducts,
        final int[] priceOfProducts, final int[] countOfProducts) {
        super(nameOfProducts, priceOfProducts, countOfProducts);
    }
}
