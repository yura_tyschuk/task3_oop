package epam.com;
import java.util.List;

/**
 * Hypermarket class has getters and setters for products,
 * return name of products, find products by price and name.
 * @author Yura Tyschuk
 * @version 1.0
 * @since  11.11.2019
 */
public class Hypermarket {


  private List<String> nameOfProducts;
  private int[] priceOfProducts;
  private int[] countOfProducts;

  /**
   * @param nameOfProducts  name of products
   * @param priceOfProducts price of products
   * @param countOfProducts count of products
   */
  Hypermarket(final List<String> nameOfProducts,
      final int[] priceOfProducts, final int[] countOfProducts) {
    this.nameOfProducts = nameOfProducts;
    this.priceOfProducts = priceOfProducts;
    this.countOfProducts = countOfProducts;
  }

  /**
   * Getters and setters for parameters.
   */
  public final List<String> getNameOfProducts() {
    return nameOfProducts;
  }

  public final void setNameOfProducts(final List<String> nameOfProducts) {
    this.nameOfProducts = nameOfProducts;
  }

  public final int[] getPriceOfProducts() {
    return priceOfProducts;
  }

  public final  void setPriceOfProducts(final int[] priceOfProducts) {
    this.priceOfProducts = priceOfProducts;
  }

  public final int[] getCountOfProducts() {
    return countOfProducts;
  }

  public final void setCountOfProducts(final int[] countOfProducts) {
    this.countOfProducts = countOfProducts;
  }

  /**
   * print names of products.
   */
  public final void products() {
    System.out.println("Name of product: " + nameOfProducts);
  }

  /**
   * Finds products by price and print all products that is equal to priceOfProductsThatNeedToBeFind
   * or less than it.
   *
   * @param priceOfProductsThatNeedToBeFind price of product
   */
  public final void findProductByPrice(int priceOfProductsThatNeedToBeFind) {
    for (int i = 0; i < nameOfProducts.size(); i++) {
      if (priceOfProductsThatNeedToBeFind >= priceOfProducts[i]) {
        System.out.println("Product: " + nameOfProducts.get(i));
      }
    }

  }

  /**
   * Find products by name if count of not matches
   * is equal to size of nameofproducts list prints
   * that products not found.
   *
   * @param productThatNeedToBeFind product that need to be find.
   */
  public final void findByName(String productThatNeedToBeFind) {
    int countOfNotMatches = 0;

    for (int i = 0; i < nameOfProducts.size(); i++) {
      if (productThatNeedToBeFind == nameOfProducts.get(i)) {
        System.out.println("Product had been found! ");
        System.out.println("Count: " + countOfProducts[i]);
        System.out.println("Price: " + priceOfProducts[i]);

      } else {
        countOfNotMatches++;
      }
    }

    if (countOfNotMatches == nameOfProducts.size()) {
      System.out.println("Product not found!");

    }
  }
}
